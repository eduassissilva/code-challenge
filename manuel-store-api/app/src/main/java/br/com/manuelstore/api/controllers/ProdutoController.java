package br.com.manuelstore.api.controllers;

import br.com.manuelstore.api.entities.Atributo;
import br.com.manuelstore.api.entities.Produto;
import br.com.manuelstore.api.repositories.AtributoRepository;
import br.com.manuelstore.api.repositories.ProdutoRepository;
import br.com.manuelstore.api.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/produtos")
@CrossOrigin("*")
public class ProdutoController {

    private static final Logger log = LoggerFactory.getLogger(ProdutoController.class);

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private AtributoRepository atributoRepository;

    @GetMapping
    public ResponseEntity<Page<Produto>> getAllProdutos(Pageable pageable) {
        Page<Produto> produtos = produtoRepository.findAll(pageable);
        return ResponseEntity.ok(produtos);
    }

    @PostMapping
    public ResponseEntity<Response<Produto>> createProduto(@Valid @RequestBody Produto produto, BindingResult result) {

        validarProduto(produto, result);

        Response<Produto> response = new Response<Produto>();

        if(result.hasErrors()) {
            log.error("Erro validando dados do produto: {}", result.getAllErrors());
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        Produto p = produtoRepository.save(produto);
        response.setData(p);

        log.error("Produto criado com sucesso.");
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping
    public ResponseEntity<Response<Produto>> updateProduto(@Valid @RequestBody Produto produto, BindingResult result) {

        log.error("Atualizacao do produto: {}", result.getAllErrors());
        Response<Produto> response = new Response<Produto>();

        if(result.hasErrors()) {
            log.error("Erro validando dados do produto: {}", result.getAllErrors());
            response.setData(produto);
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        return produtoRepository.findByCodigo(produto.getCodigo()).map(p ->{
            p.setNome(produto.getNome());
            p.setPreco(produto.getPreco());
            p.setEstoque(produto.getEstoque());
            p.setDescricao(produto.getDescricao());
            produtoRepository.save(p);
            response.setData(p);

            log.error("Produto atualizado com sucesso.");
            return ResponseEntity.ok().body(response);
        }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(response));

    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity<Response<String>> delete(@PathVariable String codigo) {

        log.error("Removendo produto de codigo: {}", codigo);

        return produtoRepository.findByCodigo(codigo).map(p ->{
            produtoRepository.delete(p.getId());
            log.error("Produto removido com sucesso.");
            return ResponseEntity.ok().body(new Response<String>());
        }).orElse(handleNotFound(codigo));
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Response<Produto>> findByCodigo(@PathVariable String codigo) {
        log.error("Buscando produto de codigo: {}", codigo);
        Response<Produto> response = new Response<Produto>();
        return produtoRepository.findByCodigo(codigo).map(p ->{
            response.setData(p);
            log.error("Produto encontrado.");
            return ResponseEntity.ok().body(response);
        }).orElse(ResponseEntity.notFound().build());
    }

    private ResponseEntity<Response<String>> handleNotFound(final String codigo) {
        Response<String> response = new Response<>();
        response.getErrors().add("Produto " + codigo + " não encontrado.");
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    private void validarProduto(Produto produto, BindingResult result) {

        Optional<Produto> p = produtoRepository.findByCodigo(produto.getCodigo());

        if(p.isPresent()) {
            result.addError(new ObjectError("produto", "Produto já cadastrado."));
        }

        if (!CollectionUtils.isEmpty(produto.getAtributos())) {
            result.addError(new ObjectError("produto", "Não é possível cadastrar produto e atributos no mesmo endpoint."));
        }
    }

    @GetMapping("/{codigo}/atributos")
    public ResponseEntity<Response<List>> getAllAtributos(@PathVariable final String codigo) {
        Response<List> response = new Response<List>();
        log.info("Lista de atributos do produto: {}", codigo);
        return produtoRepository.findByCodigo(codigo).map(p ->{
            List<Atributo> atributos = atributoRepository.findByProdutoId(p.getId());
            response.setData(atributos);
            return ResponseEntity.ok().body(response);
        }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @GetMapping("/{codigo}/atributos/{id}")
    public ResponseEntity<Response<Atributo>> getAtributoById(@PathVariable final String codigo, @PathVariable final Long id) {
        Response<Atributo> response = new Response<Atributo>();
        log.info("Buscando atributo {} do produto {}", id, codigo);
        return produtoRepository.findByCodigo(codigo).map(p ->{
            return atributoRepository.findById(id).map(atributo -> {
                response.setData(atributo);
                return ResponseEntity.ok(response);
            }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());

        }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PostMapping("/{codigo}/atributos")
    public ResponseEntity<Response<Atributo>> createAtributo(@PathVariable final String codigo, @Valid @RequestBody Atributo atributo, BindingResult result) {

        Response<Atributo> response = new Response<>();

        if(result.hasErrors()) {
            log.error("Erro validando dados do atributo: {}", result.getAllErrors());
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        return produtoRepository.findByCodigo(codigo).map(produto -> {
            log.error("Atributo adicionado ao produto, de codigo {}, com sucesso.", codigo);
            Atributo a = atributoRepository.save(atributo);
            response.setData(a);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        }).orElse(ResponseEntity.notFound().build());

    }

    @PutMapping("/{codigo}/atributos")
    public ResponseEntity<Response<Atributo>> updateAtributo(@PathVariable String codigo, @Valid @RequestBody Atributo atributo, BindingResult result) {

        log.error("Atualizacao do atributo: {}, do produto de codigo: {}", atributo.getId(), codigo);
        Response<Atributo> response = new Response<Atributo>();

        if(result.hasErrors()) {
            log.error("Erro validando dados do atributo: {}", result.getAllErrors());
            response.setData(atributo);
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        return produtoRepository.findByCodigo(codigo).map(p ->{
            return atributoRepository.findById(atributo.getId()).map(a -> {
                a.setNome(atributo.getNome());
                a.setValor(atributo.getValor());
                a.setTipo(atributo.getTipo());
                log.error("Atributo atualizado com sucesso.");
                return ResponseEntity.status(HttpStatus.CREATED).body(response);
            }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());

        }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(response));

    }

    @DeleteMapping("/{codigo}/atributos/{id}")
    public ResponseEntity<Response<Atributo>> deleteAtributo(@PathVariable final String codigo, @PathVariable final Long id) {
        log.error("Removendo atributo de id: {}", id);
        Response<Atributo> response = new Response<Atributo>();
        log.info("Removendo atributo {} do produto {}", id, codigo);
        return produtoRepository.findByCodigo(codigo).map(p ->{
            return atributoRepository.findById(id).map(atributo -> {
                atributoRepository.delete(atributo);
                log.error("Atributo removido com sucesso.");
                return ResponseEntity.ok(response);
            }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());

        }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(response));
    }

}
