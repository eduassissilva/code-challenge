package br.com.manuelstore.api.repositories;

import br.com.manuelstore.api.entities.Pedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long> {
    Optional<Pedido> findByCodigo(String codigo);
    @Query("select p from Pedido p " +
            "LEFT JOIN FETCH p.itens i " +
            "where p.dataDaCompra >= :dataInicio and p.dataDaCompra <= :dataFim")
    List<Pedido> relatorioTicketMedio(@Param("dataInicio") Date dataInicio, @Param("dataFim") Date dataFim);

    @Override
    @Query("select p from Pedido p left join fetch p.itens i")
    List<Pedido> findAll();
}