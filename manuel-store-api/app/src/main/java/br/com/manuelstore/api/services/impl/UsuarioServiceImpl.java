package br.com.manuelstore.api.services.impl;

import br.com.manuelstore.api.entities.Usuario;
import br.com.manuelstore.api.repositories.UsuarioRepository;
import br.com.manuelstore.api.services.UsuarioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	public static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Usuario persistir(Usuario usuario) {
		log.info("Persistindo usuario: {}", usuario);
		return this.usuarioRepository.save(usuario);
	}

	@Override
	public Optional<Usuario> buscarPorEmail(String email) {
		log.info("Buscando usuario pelo email: {}", email);
		return Optional.ofNullable(this.usuarioRepository.findByEmail(email));
	}

	@Override
	public Optional<Usuario> usuarioJaECadastrado(final Usuario usuario) {
		log.info("Buscando usuario por exemplo");
		Usuario usuarioExample = new Usuario();
		usuarioExample.setEmail(usuario.getEmail());
		usuarioExample.setCpf(usuario.getCpf());
        Example<Usuario> example = Example.of(usuarioExample);
		return this.usuarioRepository.findByEmailOrCpf(usuario.getEmail(), usuario.getCpf());
	}
}
