package br.com.manuelstore.api.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="produto")
public class Produto implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Códido do produto é um campo obrigatório")
    @Column(name = "codigo", nullable = false, unique = true)
    private String codigo;

    @NotNull(message = "Nome é um campo obrigatório")
    @Column(name="nome", nullable=false)
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @NotNull(message = "Estoque é um campo obrigatório")
    @Column(name = "estoque", nullable = false)
    @Min(value = 0, message = "Produto não possui estoque suficiente")
    private Integer estoque;

    @NotNull(message = "Preço é um campo obrigatório")
    @Column(name = "preco", nullable = false)
    private Float preco;

    @JsonIgnore
    @OneToMany(mappedBy = "produto", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Atributo> atributos = new ArrayList<>();

    public void addAtributo(Atributo atributo) {
        this.atributos.add(atributo);
        atributo.setProduto(this);
    }

    public void decrementaEstoque(final Integer quantidade) {
        this.estoque = this.estoque - quantidade;
    }

    public void incrementaEstoque(final Integer quantidade) {
        this.estoque = this.estoque + quantidade;
    }
}