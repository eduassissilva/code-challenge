package br.com.manuelstore.api.services;

import br.com.manuelstore.api.entities.Usuario;

import java.util.Optional;

public interface UsuarioService {

	/**
	 * Cadastra um novo usuario na base de dados.
	 * 
	 * @param usuario
	 * @return Usuario
	 */
	Usuario persistir(Usuario usuario);

	/**
	 * Busca e retorna um usuario dado um email.
	 * 
	 * @param email
	 * @return Optional<Usuario>
	 */
	Optional<Usuario> buscarPorEmail(String email);

	Optional<Usuario> usuarioJaECadastrado(Usuario usuarioExample);
}
