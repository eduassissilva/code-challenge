package br.com.manuelstore.api.entities;

import br.com.manuelstore.api.enums.PerfilEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 6197293860134491476L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="nome", nullable=false)
	private String nome;

	@Column(name="email", nullable=false, unique = true)
	private String email;

	@Column(name="senha", nullable=false)
	private String senha;

	@Column(name="cpf", nullable=false, unique = true)
	private String cpf;

	@Enumerated(EnumType.STRING)
	@Column(name="perfil", nullable=false)
	private PerfilEnum perfil;

	@Column(name="data_criacao", nullable=false)
	private Date dataCriacao;

	@Column(name="data_atualizacao", nullable=false)
	private Date dataAtualizacao;

	public Usuario() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public PerfilEnum getPerfil() {
		return perfil;
	}

	public void setPerfil(PerfilEnum perfil) {
		this.perfil = perfil;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}


	@PrePersist
	public void prePersist() {
		Date atual = new Date();
		this.dataCriacao = atual;
		this.dataAtualizacao = atual;
	}
	
	@PreUpdate
	public void preUpdate() {
		this.dataAtualizacao = new Date();
	}
	
	@Override
	public String toString() {
		return "Usuario [id=" + this.id + ", nome=" + this.nome + ", email=" + this.email + ", senha=" + this.senha + ", "
				+ "cpf=" + this.cpf + ", perfil=" + this.perfil + "dataCriacao=" + this.dataCriacao + ", "
				+ "dataAtualizacao=" + this.dataAtualizacao;
	}
}