package br.com.manuelstore.api.enums;

public enum PerfilEnum {
	ROLE_ADMIN,
	ROLE_USUARIO;
}
