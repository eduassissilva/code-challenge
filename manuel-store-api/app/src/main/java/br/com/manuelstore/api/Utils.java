package br.com.manuelstore.api;

public class Utils {

    public final static String MESSAGE = "messageTemplate=";
    public final static String PATTERN = "'}\n]";


    public static String formatConstraintViolationMessage(Throwable cause) {
        return cause.getMessage().substring(cause.getMessage().indexOf(MESSAGE) + MESSAGE.length() + 1).replace(PATTERN, "");
    }
}