package br.com.manuelstore.api.repositories;

import br.com.manuelstore.api.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional(readOnly=true)
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	Usuario findByEmail(String email);

	Optional<Usuario> findByEmailOrCpf(String email, String cpf);
}
