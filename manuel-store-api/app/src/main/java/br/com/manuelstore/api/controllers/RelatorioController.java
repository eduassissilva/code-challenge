package br.com.manuelstore.api.controllers;

import br.com.manuelstore.api.entities.Pedido;
import br.com.manuelstore.api.entities.RelatorioDTO;
import br.com.manuelstore.api.repositories.PedidoRepository;
import br.com.manuelstore.api.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/relatorio")
@CrossOrigin("*")
public class RelatorioController {

    private static final Logger LOG = LoggerFactory.getLogger(ProdutoController.class);

    @Autowired
    private PedidoRepository pedidoRepository;

    @GetMapping("/ticket-medio")
    public ResponseEntity<Response<RelatorioDTO>> ticketMedio(@RequestParam("dataInicio") @DateTimeFormat(pattern="yyyyMMdd") Date dataInicio,
                                                              @RequestParam("dataFim") @DateTimeFormat(pattern="yyyyMMdd") Date dataFim) {
        LOG.info("Buscando relatorio de ticker médio. Data1: {}, Data2 {}", dataInicio, dataFim);
        Response<RelatorioDTO> response = new Response<RelatorioDTO>();
        List<Pedido> pedidos = pedidoRepository.relatorioTicketMedio(dataInicio, dataFim);

        RelatorioDTO relatorioDTO = new RelatorioDTO();
        relatorioDTO.setValorTicketMedio(0.0f);

        if (CollectionUtils.isEmpty(pedidos)) {
            response.setData(relatorioDTO);
            return ResponseEntity.ok(response);
        }

        relatorioDTO.setValorTicketMedio( calculaTicketMedio(pedidos) );
        response.setData(relatorioDTO);
        return ResponseEntity.ok(response);
    }

    private float calculaTicketMedio(final List<Pedido> pedidos) {
        return (float) pedidos
                .stream()
                .mapToDouble(pedido -> {
                    float valorPagoNosItens = (float) pedido.getItens()
                            .stream()
                            .mapToDouble(value -> value.getPrecoDaVenda())
                            .sum();
                    return valorPagoNosItens + pedido.getValorDoFrete();
                }).sum() / pedidos.size();
    }
}
