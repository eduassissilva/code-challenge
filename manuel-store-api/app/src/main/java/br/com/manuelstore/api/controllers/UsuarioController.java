package br.com.manuelstore.api.controllers;


import br.com.manuelstore.api.entities.Usuario;
import br.com.manuelstore.api.response.Response;
import br.com.manuelstore.api.services.UsuarioService;
import br.com.manuelstore.api.utils.PasswordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/usuarios")
@CrossOrigin("*")
public class UsuarioController {

    private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    public ResponseEntity<Response<Usuario>> create(@Valid @RequestBody Usuario usuario, BindingResult result) {

        Response<Usuario> response = new Response<>();

        if(result.hasErrors()) {
            log.error("Erro validando dados do produto: {}", result.getAllErrors());
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        try {

            usuario.setSenha(PasswordUtils.gerarBCrypt(usuario.getSenha()));
            if(usuarioService.usuarioJaECadastrado(usuario).isPresent()) {
                response.getErrors().add("Usuário já cadastrado!");
                return ResponseEntity.badRequest().body(response);
            }

            Usuario u = usuarioService.persistir(usuario);

            log.info("Usuario criado com sucesso.");
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            log.error("Erro ao criar usuario .");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
    }

}
