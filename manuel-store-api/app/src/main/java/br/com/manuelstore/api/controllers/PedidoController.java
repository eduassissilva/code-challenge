package br.com.manuelstore.api.controllers;

import br.com.manuelstore.api.Utils;
import br.com.manuelstore.api.entities.Item;
import br.com.manuelstore.api.entities.Pedido;
import br.com.manuelstore.api.entities.Produto;
import br.com.manuelstore.api.repositories.PedidoRepository;
import br.com.manuelstore.api.repositories.ProdutoRepository;
import br.com.manuelstore.api.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pedidos")
@CrossOrigin("*")
public class PedidoController {

    private static final Logger LOG = LoggerFactory.getLogger(ProdutoController.class);

    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @GetMapping
    public ResponseEntity<Response<List<Pedido>>> findAll() {
        LOG.info("Buscando pedidos");
        Response<List<Pedido>> response = new Response<>();
        response.setData(pedidoRepository.findAll());
        return ResponseEntity.ok().body(response);
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response<Pedido>> create(@Valid @RequestBody Pedido pedido, BindingResult result) {

        Response<Pedido> response = new Response<>();

        try {

            validaPedido(pedido, result);
            List<Produto> produtos = buscaEValidaProdutos(pedido, result);

            if (result.hasErrors()) {
                LOG.error("Erro validando dados do pedido: {}", result.getAllErrors());
                result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
                return ResponseEntity.badRequest().body(response);
            }

            reservaEstoqueEAtualizaPrecoDoItem(pedido, produtos);

            produtoRepository.save(produtos);
            Pedido p = pedidoRepository.save(pedido);

            response.setData(p);

            LOG.info("Produto criado com sucesso.");
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            String message = handleException(e);
            response.getErrors().add(message);
            LOG.error("Erro ao cadastrar o pedido. Message: ", StringUtils.isEmpty(message) ? e.getMessage() : message);

            if (!StringUtils.isEmpty(message) )
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

            return ResponseEntity.badRequest().body(response);
        }

    }

    private void validaPedido(final Pedido pedido, BindingResult result) {

        this.pedidoRepository.findByCodigo(pedido.getCodigo()).ifPresent(p -> {
            result.addError(new ObjectError("pedido", "Código de pedido " + p.getCodigo() + " já cadastrado."));
        });

    }

    private String handleException(Exception e) {

        if (e instanceof TransactionSystemException && e.getCause() != null && e.getCause().getCause() != null) {
            Throwable cause = e.getCause().getCause();
            if (cause instanceof ConstraintViolationException)
                return Utils.formatConstraintViolationMessage(cause);
        }
        return "";
    }

    private void reservaEstoqueEAtualizaPrecoDoItem(final Pedido pedido, List<Produto> produtos) {
        if (produtos != null) {
            pedido
                    .getItens()
                    .stream()
                    .forEach(item -> {
                        produtos
                                .stream()
                                .filter(produto -> produto.getCodigo().equals(item.getCodigoDoProduto()))
                                .findFirst()
                                .ifPresent(p -> {
                                    p.decrementaEstoque(item.getQuantidade());
                                    item.setPrecoDaVenda(p.getPreco() * item.getQuantidade());
                                });
                    });
        }
    }

    private void atualizaEstoqueEItem(final Pedido pedidoAtual, final Pedido novoPedido, List<Produto> produtos) {
        if (produtos != null) {
            novoPedido
                    .getItens()
                    .stream()
                    .forEach(item -> {
                        produtos
                                .stream()
                                .filter(produto -> produto.getCodigo().equals(item.getCodigoDoProduto()))
                                .findFirst()
                                .ifPresent(p -> {
                                    p.decrementaEstoque(item.getQuantidade());
                                    item.setPrecoDaVenda(p.getPreco() * item.getQuantidade());
                                });
                    });
        }
    }


    private List<Produto> buscaEValidaProdutos(final Pedido pedido, BindingResult result) {
        List<String> codigos = pedido
                .getItens()
                .stream()
                .map(item -> item.getCodigoDoProduto())
                .collect(Collectors.toList());

        List<Produto> produtos = produtoRepository.findByCodigoIn(codigos);

        if(produtos.size() != codigos.size()) {
            result.addError(new ObjectError("pedido", "Não é possível criar um pedido com um item não cadastrado."));
            return null;
        }
        return produtos;
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Response<Pedido>> findByCodigo(@PathVariable String codigo) {
        LOG.info("Buscando produto de codigo: {}", codigo);
        Response<Pedido> response = new Response<>();
        return pedidoRepository.findByCodigo(codigo).map(p ->{
            response.setData(p);
            LOG.info("Pedido encontrado.");
            return ResponseEntity.ok().body(response);
        }).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping
    public ResponseEntity<Response<Pedido>> updateProduto(@Valid @RequestBody Pedido pedido, BindingResult result) {

        LOG.error("Atualizacao do pedido: {}", result.getAllErrors());
        Response<Pedido> response = new Response<Pedido>();

        List<Produto> produtos = buscaEValidaProdutos(pedido, result);

        if(result.hasErrors()) {
            LOG.error("Erro validando dados do pedido: {}", result.getAllErrors());
            response.setData(pedido);
            result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(response);
        }

        try {
            return pedidoRepository.findByCodigo(pedido.getCodigo()).map(p ->{
                liberaEstoque(p, pedido);
                reservaEstoqueEAtualizaPrecoDoItem(pedido, produtos);
                Pedido pedidoAtualizado = updatePedido(p, pedido);
                response.setData(pedidoAtualizado);
                LOG.info("Produto atualizado com sucesso.");
                return ResponseEntity.ok().body(response);
            }).orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(response));

        } catch (Exception e) {
            String message = handleException(e);
            response.getErrors().add(message);
            LOG.error("Erro ao atualizar o pedido. Message: ", StringUtils.isEmpty(message) ? e.getMessage() : message);

            if (!StringUtils.isEmpty(message) )
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);

            return ResponseEntity.badRequest().body(response);
        }

    }

    private void liberaEstoque(Pedido pedidoAtual, Pedido pedidoAtualizado) {


        List<Item> itensRemovidos = pedidoAtual
                .getItens()
                .stream()
                .filter(item -> {
                    return pedidoAtualizado.getItens().stream().noneMatch(itemAtualizado -> item.getId().equals(itemAtualizado.getId()));
                })
                .collect(Collectors.toList());

        List<String> codigos = itensRemovidos
                .stream()
                .map(item -> item.getCodigoDoProduto())
                .collect(Collectors.toList());

        List<Produto> produtos = produtoRepository.findByCodigoIn(codigos);

        produtos
                .stream()
                .forEach(produto -> {
                    itensRemovidos
                            .stream()
                            .forEach(item -> {
                                if(item.getCodigoDoProduto().equals(produto.getCodigo())) {
                                    produto.incrementaEstoque(item.getQuantidade());
                                    item.setPedido(null);
                                }
                            });
                });

        produtoRepository.save(produtos);
    }

    private Pedido updatePedido(Pedido pedidoAtual, Pedido pedidoNovo) {
        pedidoAtual.setValorDoFrete(pedidoNovo.getValorDoFrete());
        pedidoAtual.setNomeDoComprador(pedidoNovo.getNomeDoComprador());
        pedidoAtual.setEstado(pedidoNovo.getEstado());
        pedidoAtual.setItens(pedidoNovo.getItens());
        return this.pedidoRepository.save(pedidoAtual);
    }

}