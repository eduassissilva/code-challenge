package br.com.manuelstore.api.serializers;


import br.com.manuelstore.api.entities.Item;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ItemSerializer extends StdSerializer<List<Item>> {

    public ItemSerializer() {
        this(null);
    }
    public ItemSerializer(Class<List<Item>> t) {
        super(t);
    }

    @Override
    public void serialize(List<Item> items, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        jsonGenerator.writeObject(
            items
                .stream()
                .map(item -> {
                    Item i = new Item();
                    i.setId(item.getId());
                    i.setCodigoDoProduto(item.getCodigoDoProduto());
                    i.setQuantidade(item.getQuantidade());
                    i.setPrecoDaVenda(item.getPrecoDaVenda());
                    return i;
                }).collect(Collectors.toList())
        );

    }
}
