package br.com.manuelstore.api.config;

import br.com.manuelstore.api.entities.Usuario;
import br.com.manuelstore.api.enums.PerfilEnum;
import br.com.manuelstore.api.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import br.com.manuelstore.api.security.utils.JwtTokenUtil;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@Profile("dev")
@EnableSwagger2
public class SwaggerConfig {

	public static final String EMAIL_USER_ADMIN = "admin@admin.com";
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private UsuarioService usuarioService;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("br.com.manuelstore.api.controllers"))
				.paths(PathSelectors.any()).build()
				.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Manuel Store API")
				.description("Documentação da API de acesso aos endpoints da Loja do seu Manuel")
				.version("0.1")
				.build();
	}
	
	@Bean
	public SecurityConfiguration security() {
		String token;
		
		try {
			createAdminUser();
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(EMAIL_USER_ADMIN);
			token = this.jwtTokenUtil.obterToken(userDetails);
		} catch (Exception e) {
			token = "";
		}
		
		return new SecurityConfiguration(null, null, null, null, "Bearer " + token, ApiKeyVehicle.HEADER, "Authorization", ",");
	}

	private void createAdminUser() {
		Usuario admin = new Usuario();
		admin.setEmail(EMAIL_USER_ADMIN);
		admin.setSenha("$2a$06$xIvBeNRfS65L1N17I7JzgefzxEuLAL0Xk0wFAgIkoNqu9WD6rmp4m");
		admin.setCpf("121.290.920-80");
		admin.setNome("Admin");
		admin.setPerfil(PerfilEnum.ROLE_ADMIN);
		this.usuarioService.persistir(admin);
	}
}
