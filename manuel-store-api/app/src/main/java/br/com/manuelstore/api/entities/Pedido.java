package br.com.manuelstore.api.entities;

import br.com.manuelstore.api.serializers.ItemSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.DoubleStream;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="pedido")
public class Pedido {

    public enum Estado {

        NOVO("NOVO"),
        APROVADO("APROVADO"),
        ENTREGUE("ENTREGUE"),
        CANCELADO("CANCELADO");

        private final String valor;

        private Estado(String tipo) {
            this.valor = tipo;
        }
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Código é um campo obrigatório")
    @Column(name = "codigo", nullable = false, unique = true)
    private String codigo;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_compra", nullable = false)
    private Date dataDaCompra;

    @NotNull(message = "Nome do comprador é um campo obrigatório")
    @Column(name = "nome_comprador", nullable = false)
    private String nomeDoComprador;

    @NotNull(message = "Estado é um campo obrigatório")
    @Column(name = "estado", nullable = false)
    @Enumerated
    private Estado estado;

    @NotNull(message = "Valor do frete é um campo obrigatório")
    @Column(name = "valor_frete", nullable = false)
    private Float valorDoFrete;

    @Size(min = 1, message = "É necessário pelo menos um item para gerar um pedido.")
    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonSerialize(using = ItemSerializer.class)
    private List<Item> itens;

    @PrePersist
    public void prePersist() {
        Date atual = new Date();
        this.dataDaCompra = atual;
        if (!CollectionUtils.isEmpty(this.getItens())) {
            this.getItens().stream().forEach(item -> item.setPedido(this));
        }
    }

    public void addItem(Item item) {
        if (this.itens == null) {
            this.itens = new ArrayList<>();
        }

        this.itens.add(item);
        item.setPedido(this);
    }
}