package br.com.manuelstore.api.repositories;

import br.com.manuelstore.api.entities.Atributo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AtributoRepository extends JpaRepository<Atributo, Long> {
    List<Atributo> findByProdutoId(Long produtoId);
    Optional<Atributo> findById(Long id);
}