package br.com.manuelstore.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class ManuelStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManuelStoreApplication.class, args);
	}
}
