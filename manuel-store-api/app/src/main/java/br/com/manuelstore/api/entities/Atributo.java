package br.com.manuelstore.api.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="atributo")
public class Atributo {

    public enum Tipo {
        NUMERICO("NUMERICO"),
        STRING("STRING"),
        BOOLEAN("BOOLEAN");

        private final String valor;

        private Tipo(String tipo) {
            this.valor = tipo;
        }
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Nome é um campo brigatório")
    @Column(name="nome", nullable=false)
    private String nome;

    @NotNull(message = "Valor é um campo obrigatório")
    @Column(name="valor", nullable=false)
    private String valor;

    @NotNull(message = "Tipo é um campo obrigatório")
    @Enumerated
    @Column(name="tipo", nullable=false)
    private Tipo tipo;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "produto_id", updatable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Produto produto;
}
