package br.com.manuelstore.api.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="item")
public class Item {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Código do produto é um campo obrigatório")
    @Column(name = "codigo_produto", nullable = false)
    private String codigoDoProduto;

    @NotNull(message = "Quantidade é um campo obrigatório")
    @Column(name = "quantidade", nullable = false)
    private Integer quantidade;


    @NotNull(message = "Preço da venda é um campo obrigatório")
    @Column(name = "preco_venda", nullable = false)
    private Float precoDaVenda;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pedido_id", updatable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Pedido pedido;

}