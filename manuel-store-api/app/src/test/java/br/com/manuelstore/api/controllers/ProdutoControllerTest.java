package br.com.manuelstore.api.controllers;

import br.com.manuelstore.api.entities.Atributo;
import br.com.manuelstore.api.entities.Produto;
import br.com.manuelstore.api.repositories.AtributoRepository;
import br.com.manuelstore.api.repositories.ProdutoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Optional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ProdutoControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProdutoRepository produtoRepository;

    @MockBean
    private AtributoRepository atributoRepository;

    private static final String URL_BASE_PRODUTOS = "/api/produtos/";
    private static final String URL_SUFIX_ATRIBUTO = "/atributos/";

    @Test
    @WithMockUser
    public void testBuscarTodosProdutos() throws Exception {

        ArrayList<Produto> produtos = new ArrayList<>();
        produtos.add(new Produto());
        produtos.add(new Produto());
        Page<Produto> pagedResponse = new PageImpl(produtos);

        BDDMockito.given(this.produtoRepository.findAll((org.springframework.data.domain.Pageable) Mockito.any())).willReturn(pagedResponse);

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", Matchers.hasSize(2)));
    }

    @Test
    @WithMockUser
    public void testBuscarProdutoInexistentePeloCodigo() throws Exception {

        final String codigo = "PRD0001";

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS + codigo)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void testBuscarProdutoPeloCodigo() throws Exception {

        final String codigo = "PRD0001";

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(new Produto())));

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS + codigo)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testNaoDeveSalvarProdutoInvalido() throws Exception {
        Produto produto = new Produto();
        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders.post(URL_BASE_PRODUTOS)
                .content(new ObjectMapper().writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors", Matchers.hasSize(4)));
    }

    @Test
    @WithMockUser
    public void testNaoDeveSalvarProdutoComCodigoJaExistente() throws Exception {

        Produto produto = new Produto();
        produto.setCodigo("PRD0001");
        produto.setNome("Teste");
        produto.setDescricao("Produto de teste");
        produto.setEstoque(20);
        produto.setPreco(200.00f);
        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));

        mvc.perform(MockMvcRequestBuilders.post(URL_BASE_PRODUTOS)
                .content(new ObjectMapper().writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors", Matchers.hasSize(1)));
    }

    @Test
    @WithMockUser
    public void testDeveSalvarProduto() throws Exception {
        Produto produto = new Produto();
        produto.setCodigo("PRD0001");
        produto.setNome("Teste");
        produto.setDescricao("Produto de teste");
        produto.setEstoque(20);
        produto.setPreco(200.00f);

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .post(URL_BASE_PRODUTOS)
                .content(new ObjectMapper().writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    public void testNaoDeveAtualizarProdutoInexistente() throws Exception {
        Produto produto = new Produto();
        produto.setCodigo("PRD0001");
        produto.setNome("Teste");
        produto.setDescricao("Produto de teste");
        produto.setEstoque(20);
        produto.setPreco(200.00f);

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .put(URL_BASE_PRODUTOS)
                .content(new ObjectMapper().writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void testDeveAtualizarProduto() throws Exception {
        Produto produto = new Produto();
        produto.setCodigo("PRD0001");
        produto.setNome("Teste");
        produto.setDescricao("Produto de teste");
        produto.setEstoque(20);
        produto.setPreco(200.00f);

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));

        mvc.perform(MockMvcRequestBuilders
                .put(URL_BASE_PRODUTOS)
                .content(new ObjectMapper().writeValueAsString(produto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testRemoverProduto() throws Exception {

        Produto produto = new Produto();
        produto.setId(1l);
        produto.setCodigo("PRD0001");
        produto.setNome("Teste");
        produto.setDescricao("Produto de teste");
        produto.setEstoque(20);
        produto.setPreco(200.00f);

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));

        mvc.perform(MockMvcRequestBuilders.delete(URL_BASE_PRODUTOS + produto.getCodigo())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testNaoDeveRemoverProdutoInexistente() throws Exception {

        Produto produto = new Produto();
        produto.setId(1l);
        produto.setCodigo("PRD0001");
        produto.setNome("Teste");
        produto.setDescricao("Produto de teste");
        produto.setEstoque(20);
        produto.setPreco(200.00f);

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders.delete(URL_BASE_PRODUTOS + produto.getCodigo())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void testBuscarTodosAtributosPorProduto() throws Exception {
    final String codigoDoProduto = "PRD0001";
        ArrayList<Atributo> atributos = new ArrayList<>();
        atributos.add(new Atributo());
        atributos.add(new Atributo());

        BDDMockito.given(this.produtoRepository.findByCodigo(codigoDoProduto)).willReturn(Optional.of(new Produto()));
        BDDMockito.given(this.atributoRepository.findByProdutoId(Mockito.anyLong())).willReturn(atributos);

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS + codigoDoProduto + URL_SUFIX_ATRIBUTO)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testBuscarAtributosDeProdutoInexistente() throws Exception {

        final String codigo = "PRD0001";

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS + codigo + URL_SUFIX_ATRIBUTO)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void testBuscarAtributosPorId() throws Exception {

        final String codigo = "PRD0001";
        Produto produto = new Produto();
        produto.setCodigo(codigo);

        Atributo atributo = new Atributo();

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));
        BDDMockito.given(this.atributoRepository.findById(Mockito.anyLong())).willReturn((Optional.of(atributo)));

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS + produto.getCodigo() + URL_SUFIX_ATRIBUTO)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void deveSalvarAtributo() throws Exception {

        final String codigo = "PRD0001";
        Produto produto = new Produto();

        Atributo atributo = new Atributo();
        atributo.setTipo(Atributo.Tipo.NUMERICO);
        atributo.setValor("100");
        atributo.setNome("Peso");

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));

        mvc.perform(MockMvcRequestBuilders
                .post(URL_BASE_PRODUTOS + codigo + URL_SUFIX_ATRIBUTO)
                .content(new ObjectMapper().writeValueAsString(atributo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    public void deveSalvarAtributoInvalido() throws Exception {

        final String codigo = "PRD0001";
        Produto produto = new Produto();
        Atributo atributo = new Atributo();

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));

        mvc.perform(MockMvcRequestBuilders
                .post(URL_BASE_PRODUTOS + codigo + URL_SUFIX_ATRIBUTO)
                .content(new ObjectMapper().writeValueAsString(atributo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors", Matchers.hasSize(3)));
    }

    @Test
    @WithMockUser
    public void naoDeveSalvarAtributoParaUmProdutoInexistente() throws Exception {

        final String codigo = "PRD0001";
        Atributo atributo = new Atributo();
        atributo.setTipo(Atributo.Tipo.NUMERICO);
        atributo.setValor("100");
        atributo.setNome("Peso");


        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .post(URL_BASE_PRODUTOS + codigo + URL_SUFIX_ATRIBUTO)
                .content(new ObjectMapper().writeValueAsString(atributo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void naoDeveAtualizarAtributoDeProdutoInexistente() throws Exception {
        Produto produto = new Produto();
        produto.setCodigo("PRD0001");

        Atributo atributo = new Atributo();
        atributo.setTipo(Atributo.Tipo.NUMERICO);
        atributo.setValor("100");
        atributo.setNome("Peso atualizado");


        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .put(URL_BASE_PRODUTOS + produto.getCodigo() + URL_SUFIX_ATRIBUTO)
                .content(new ObjectMapper().writeValueAsString(atributo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void naoDeveAtualizarAtributoInvalido() throws Exception {
        Produto produto = new Produto();
        produto.setCodigo("PRD0001");

        Atributo atributo = new Atributo();

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .put(URL_BASE_PRODUTOS + produto.getCodigo() + URL_SUFIX_ATRIBUTO)
                .content(new ObjectMapper().writeValueAsString(atributo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors", Matchers.hasSize(3)));
    }

    @Test
    @WithMockUser
    public void deveAtualizarAtributo() throws Exception {
        Produto produto = new Produto();
        produto.setCodigo("PRD0001");

        Atributo atributo = new Atributo();
        atributo.setTipo(Atributo.Tipo.NUMERICO);
        atributo.setValor("100");
        atributo.setNome("Peso atualizado");

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));
        BDDMockito.given(this.atributoRepository.findById(Mockito.anyLong())).willReturn((Optional.of(atributo)));

        mvc.perform(MockMvcRequestBuilders
                .put(URL_BASE_PRODUTOS + produto.getCodigo() + URL_SUFIX_ATRIBUTO)
                .content(new ObjectMapper().writeValueAsString(atributo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    public void testNaoDeveRemoverAtributoDeProdutoInexistente() throws Exception {

        Produto produto = new Produto();
        produto.setCodigo("PRD0001");

        Atributo atributo = new Atributo();
        atributo.setId(1l);
        atributo.setTipo(Atributo.Tipo.NUMERICO);
        atributo.setValor("100");
        atributo.setNome("Peso atualizado");

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .delete(URL_BASE_PRODUTOS + produto.getCodigo() + URL_SUFIX_ATRIBUTO + atributo.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void testNaoDeveRemoverAtributoInexistente() throws Exception {

        Produto produto = new Produto();
        produto.setCodigo("PRD0001");

        Atributo atributo = new Atributo();
        atributo.setId(1l);
        atributo.setTipo(Atributo.Tipo.NUMERICO);
        atributo.setValor("100");
        atributo.setNome("Peso atualizado");

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(produto)));
        BDDMockito.given(this.atributoRepository.findById(Mockito.anyLong())).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .delete(URL_BASE_PRODUTOS + produto.getCodigo() + URL_SUFIX_ATRIBUTO + atributo.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void testDeveRemoverAtributo() throws Exception {

        Produto produto = new Produto();
        produto.setCodigo("PRD0001");

        Atributo atributo = new Atributo();
        atributo.setId(1l);
        atributo.setTipo(Atributo.Tipo.NUMERICO);
        atributo.setValor("100");
        atributo.setNome("Peso atualizado");


        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.anyString())).willReturn((Optional.of(produto)));
        BDDMockito.given(this.atributoRepository.findById(Mockito.anyLong())).willReturn(Optional.of(atributo));

        mvc.perform(MockMvcRequestBuilders
                .delete(URL_BASE_PRODUTOS + produto.getCodigo() + URL_SUFIX_ATRIBUTO + atributo.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void naoDeveRetornarAtributoDeProdutoInexistente() throws Exception {

        final String codigo = "PRD0001";
        final Long idAtributo = 1l;

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.anyString())).willReturn(Optional.empty());

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS + codigo + URL_SUFIX_ATRIBUTO + idAtributo)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void naoDeveRetornarAtributoInexistente() throws Exception {

        final String codigo = "PRD0001";
        final Long idAtributo = 1l;

        BDDMockito.given(this.produtoRepository.findByCodigo(Mockito.anyString())).willReturn(Optional.of(new Produto()));
        BDDMockito.given(this.atributoRepository.findById(Mockito.anyLong())).willReturn(Optional.of(new Atributo()));

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PRODUTOS + codigo + URL_SUFIX_ATRIBUTO + idAtributo)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}