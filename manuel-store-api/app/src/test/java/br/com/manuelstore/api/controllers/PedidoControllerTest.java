package br.com.manuelstore.api.controllers;

import br.com.manuelstore.api.entities.Item;
import br.com.manuelstore.api.entities.Pedido;
import br.com.manuelstore.api.entities.Produto;
import br.com.manuelstore.api.repositories.PedidoRepository;
import br.com.manuelstore.api.repositories.ProdutoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@EnableWebMvc
public class PedidoControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProdutoRepository produtoRepository;

    @MockBean
    private PedidoRepository pedidoRepository;

    private static final String URL_BASE_PEDIDOS = "/api/pedidos/";

    private static final String CODIGO_PRODUTO_PRD0001 = "PRD0001";
    private static final String CODIGO_PRODUTO_PRD0002 = "PRD0002";

    @Test
    @WithMockUser
    public void deveCriarUmPedido() throws Exception {

        List<Produto> produtos = preparaProdutos();

        Pedido pedido = geraPedido(produtos);

        BDDMockito.given(this.produtoRepository.findByCodigoIn(Mockito.any())).willReturn((produtos));

        mvc.perform(MockMvcRequestBuilders
                .post(URL_BASE_PEDIDOS)
                .content(new ObjectMapper().writeValueAsString(pedido))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }

    private Pedido geraPedido(List<Produto> produtos) {
        Pedido pedido = new Pedido();

        pedido.setCodigo("PED00001");
        pedido.setEstado(Pedido.Estado.NOVO);
        pedido.setNomeDoComprador("Eduardo Silva");
        pedido.setValorDoFrete(10.0f);

        Item item = new Item();
        item.setCodigoDoProduto(CODIGO_PRODUTO_PRD0001);
        item.setQuantidade(10);

        Item item2 = new Item();
        item2.setCodigoDoProduto(CODIGO_PRODUTO_PRD0002);
        item2.setQuantidade(5);

        new ArrayList<Item>() {{
            add(item);
            add(item2);
        }}
                .stream()
                .forEach(pedido::addItem);
        return pedido;
    }

    private List<Produto> preparaProdutos() {

        Produto produto = new Produto();
        produto.setCodigo(CODIGO_PRODUTO_PRD0001);
        produto.setNome("Teste");
        produto.setDescricao("Produto de teste");
        produto.setEstoque(20);
        produto.setPreco(200.00f);

        Produto produto2 = new Produto();
        produto2.setCodigo(CODIGO_PRODUTO_PRD0002);
        produto2.setNome("Teste");
        produto2.setDescricao("Produto de teste");
        produto2.setEstoque(20);
        produto2.setPreco(200.00f);


        return new ArrayList<Produto>() {{
            add(produto);
            add(produto2);
        }};

    }

    @Test
    @WithMockUser
    public void testBuscarPedidoPeloCodigo() throws Exception {

        List<Produto> produtos = preparaProdutos();
        Pedido p = geraPedido(produtos);

        BDDMockito.given(this.pedidoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.of(p)));

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PEDIDOS + p.getCodigo())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testBuscarPedidoInexistente() throws Exception {

        final String codigo = "PED001";
        BDDMockito.given(this.pedidoRepository.findByCodigo(Mockito.any(String.class))).willReturn((Optional.empty()));

        mvc.perform(MockMvcRequestBuilders
                .get(URL_BASE_PEDIDOS + codigo)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    public void deveAtualizarUmPedido() throws Exception {

        List<Produto> produtos = preparaProdutos();

        Pedido pedido = geraPedido(produtos);
        pedido.setId(1l);
        pedido.getItens().get(0).setId(1l);
        pedido.getItens().get(1).setId(2l);

        BDDMockito.given(this.produtoRepository.findByCodigoIn(Mockito.anyList())).willReturn((new ArrayList<Produto>(){{
            add(produtos.get(1));
        }}));

        BDDMockito.given(this.pedidoRepository.findByCodigo(Mockito.anyString())).willReturn(Optional.of(pedido));

        Pedido novoPedido = geraPedido(produtos);
        novoPedido.getItens().get(0).setId(1l);
        novoPedido.getItens().get(1).setId(2l);
        novoPedido.getItens().remove(0);
        novoPedido.getItens().get(0).setQuantidade(2);

        mvc.perform(MockMvcRequestBuilders
                .put(URL_BASE_PEDIDOS)
                .content(new ObjectMapper().writeValueAsString(novoPedido))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

    }

}
