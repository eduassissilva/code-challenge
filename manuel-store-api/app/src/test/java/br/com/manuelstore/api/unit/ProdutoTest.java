package br.com.manuelstore.api.unit;

import br.com.manuelstore.api.entities.Atributo;
import br.com.manuelstore.api.entities.Produto;
import br.com.manuelstore.api.repositories.ProdutoRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ProdutoTest {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Test
    public void deveCriarProduto() {
        Produto p = new Produto();
        p.setCodigo("PR0001");
        p.setNome("Produto A");
        p.setDescricao("Descrição Produto A");
        p.setEstoque(100);
        p.setPreco(90.99f);
        assertNotNull(p);
    }

    @Test
    public void deveCriarNovoAttributo() {
        Produto p = new Produto();
        p.setCodigo("PR0002");
        p.setNome("Produto B");
        p.setDescricao("Descrição Produto B");
        p.setEstoque(10);
        p.setPreco(19.99f);

        Atributo atributo = new Atributo();
        atributo.setNome("Cor");
        atributo.setValor("Amarelo");
        atributo.setTipo(Atributo.Tipo.STRING);
        p.addAtributo(atributo);

        assertNotNull(atributo);
        assertFalse(p.getAtributos().isEmpty());
        assertEquals(1, p.getAtributos().size());
    }

    @Test
    public void devePersistirProduto() {

        produtoRepository.deleteAll();

        Produto p = new Produto();
        p.setCodigo("PR0003");
        p.setNome("Produto C");
        p.setDescricao("Descrição Produto C");
        p.setEstoque(10);
        p.setPreco(19.99f);

        Atributo atributo = new Atributo();
        atributo.setNome("Cor");
        atributo.setValor("Amarelo");
        atributo.setTipo(Atributo.Tipo.STRING);
        p.addAtributo(atributo);

        produtoRepository.save(p);

        assertEquals(1, produtoRepository.findAll().size());
    }
}